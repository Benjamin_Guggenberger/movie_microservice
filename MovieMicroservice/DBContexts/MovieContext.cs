﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MovieMicroservice.Models;

namespace MovieMicroservice.DBContexts
{
    public class MovieContext : DbContext
    {
        public MovieContext(DbContextOptions<MovieContext> options) : base(options)
        {
        }

        public DbSet<Movie> Movies { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // initially fill database on creation
            modelBuilder.Entity<Movie>().HasData(
                new Movie
                {
                    Id = 1,
                    Name = "Interstellar",
                    Genre = "Science Fiction",
                    ReleaseYear = 2014,
                    AgeLimit = 12
                },
                new Movie
                {
                    Id = 2,
                    Name = "The Lion King",
                    Genre = "Animation",
                    ReleaseYear = 1994,
                    AgeLimit = 0
                },
                new Movie
                {
                    Id = 3,
                    Name = "Gladiator",
                    Genre =  "Action",
                    ReleaseYear = 2000,
                    AgeLimit = 16
                },
                new Movie
                {
                    Id = 4,
                    Name = "The Shining",
                    Genre = "Horror",
                    ReleaseYear = 1980,
                    AgeLimit = 16
                },
                new Movie
                {
                    Id = 5,
                    Name = "American Pie",
                    Genre = "Comedy",
                    ReleaseYear = 1999,
                    AgeLimit = 12
                });
        }
    }
}
