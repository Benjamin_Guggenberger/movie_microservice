﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MovieMicroservice.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Movies",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Genre = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ReleaseYear = table.Column<int>(type: "int", nullable: false),
                    AgeLimit = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movies", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "AgeLimit", "Genre", "Name", "ReleaseYear" },
                values: new object[,]
                {
                    { 1, 12, "Science Fiction", "Interstellar", 2014 },
                    { 2, 0, "Animation", "The Lion King", 1994 },
                    { 3, 16, "Action", "Gladiator", 2000 },
                    { 4, 16, "Horror", "The Shining", 1980 },
                    { 5, 12, "Comedy", "American Pie", 1999 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Movies");
        }
    }
}
