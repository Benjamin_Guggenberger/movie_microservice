﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MovieMicroservice.DBContexts;
using MovieMicroservice.Models;

namespace MovieMicroservice.Repositories
{
    public interface IMovieRepository
    {
        Task<ICollection<Movie>> GetAllMovies();
        Task<Movie> GetMovieById(int movieId);
        Task AddMovie(Movie movie);
    }

    public class MovieRepository : IMovieRepository
    {
        private readonly MovieContext _dbContext;
        private readonly ILogger<MovieRepository> _logger;

        public MovieRepository(MovieContext dbContext, ILogger<MovieRepository> logger)
        {
            _dbContext = dbContext;
            _logger = logger;
        }

        public async Task<ICollection<Movie>> GetAllMovies()
        {
            _logger.LogInformation("Getting all movies...");
            var allMovies = await _dbContext.Movies.ToListAsync();
            _logger.LogInformation("Loaded all movies!");

            return allMovies;
        }

        public async Task<Movie> GetMovieById(int movieId)
        {
            _logger.LogInformation($"Getting movie with id {movieId}...");
            var movie = await _dbContext.Movies.FindAsync(movieId);
            _logger.LogInformation($"Loaded movie with id {movieId}!");

            return movie;
        }

        public async Task AddMovie(Movie movie)
        {
            // auto-increment
            movie.Id = 0;

            _logger.LogInformation($"Saving movie {movie.Name}...");
            _dbContext.Add(movie);
            await _dbContext.SaveChangesAsync();
            _logger.LogInformation($"Saved movie {movie.Name}!");
        }
    }
}
