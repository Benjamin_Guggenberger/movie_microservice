﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using MovieMicroservice.Models;
using MovieMicroservice.Repositories;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MovieMicroservice.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class MovieController : ControllerBase
    {
        private readonly IMovieRepository _movieRepository;

        public MovieController(IMovieRepository movieRepository)
        {
            _movieRepository = movieRepository;
        }

        // GET: <MovieController>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var movies = await _movieRepository.GetAllMovies();
            if (movies != null)
            {
                return new OkObjectResult(movies);
            }
            return new NotFoundResult();
        }

        // GET <MovieController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var movie = await _movieRepository.GetMovieById(id);
            if (movie != null)
            {
                return new OkObjectResult(movie);
            }
            return new NotFoundResult();
        }

        // POST <MovieController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Movie movie)
        {
            await _movieRepository.AddMovie(movie);
            return new OkObjectResult(movie);
        }
    }
}
